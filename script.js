const changeThemeButton = document.querySelector(`[data-change-theme="dark"]`);
const pageBody = document.querySelector('.page__body');

let isDarkTheme;

function changeToDarkTheme() {
    pageBody.style.background = 'gray';
    changeThemeButton.innerText = 'Light theme';
    changeThemeButton.style.color = 'rgb(255, 255, 255)';
}

function changeToLightTheme() {
    pageBody.style.background = 'rgb(255, 255, 255)';
    changeThemeButton.innerText = 'Dark theme';
    changeThemeButton.style.color = 'gray';
}

if (localStorage.getItem('darkTheme') === 'true') {
    isDarkTheme = true
    changeToDarkTheme()
} else {
    localStorage.setItem('darkTheme', 'false');
    isDarkTheme = false
    changeToLightTheme()
}

changeThemeButton.addEventListener('click', () => {
    if (!isDarkTheme) {
        localStorage.setItem('darkTheme', 'true');
        isDarkTheme = true;
        changeToDarkTheme()
    } else {
        console.log(isDarkTheme + 2)
        localStorage.setItem('darkTheme', 'false');
        isDarkTheme = false;
        changeToLightTheme()
    }
})